<?
	if (!current_user_can('manage_options')) {
	    return;
	}
?>
<div class="wrap">
	view.php
    <h1><?= esc_html(get_admin_page_title()); ?></h1>
    <form action="options.php" method="post">
        <?php
        settings_fields('sp_doclib_options');
        do_settings_sections('sp_doclib');
        submit_button('Save Settings');
        ?>
    </form>
</div>
