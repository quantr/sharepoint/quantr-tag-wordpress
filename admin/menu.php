<?
function sp_doclib_create_menu(){
    $first=plugin_dir_path(__FILE__) . 'mainSetting.php';
    add_menu_page(
        'SharePoint Admin',
        'SharePoint',
        'manage_options',
        'quantr_tag_wordpress_mainMenuSlug',
        'sharepoint main menu',
        plugin_dir_url(__FILE__) . 'images/sharepoint.png'
    );

    /*add_submenu_page(
        $first,
        'Credential',
        'Credential',
        'manage_options',
        $first,
        null
    );*/

	add_submenu_page(
        'quantr_tag_wordpress_mainMenuSlug',
        'Credential',
        'Credential',
        'manage_options',
        'Credential',
        'quantr_tag_crendentialPageInit'
    );

    add_submenu_page(
        'quantr_tag_wordpress_mainMenuSlug',
        'Quantr link',
        'Quantr link',
        'manage_options',
        'Quantr link',
        'quantr_tag_quantrLinkPageInit'
    );

    /*add_submenu_page(
        'quantr_tag_wordpress_mainMenuSlug',
        'Test caml',
        'Test caml',
        'manage_options',
        plugin_dir_path(__FILE__) . 'testCaml.php',
        null
    );*/

	remove_submenu_page('quantr_tag_wordpress_mainMenuSlug', 'quantr_tag_wordpress_mainMenuSlug');
}

function quantr_tag_crendentialPageInit()
{
    if (!current_user_can('manage_options')) {
        return;
    }
?>
	<form method="post" action="options.php">
<?
    settings_fields('credential_option_group');  // option_group
    do_settings_sections('Credential'); // page
    submit_button('Save Settings');
?>
	</form>
<?
}

function quantr_tag_quantrLinkPageInit()
{
    if (!current_user_can('manage_options')) {
        return;
    }
	?>
		<form method="post" action="options.php">
	<?
	    settings_fields('quantrLink_option_group');  // option_group
	    do_settings_sections('Quantr link'); // page
	    submit_button('Save Settings');
	?>
		</form>
	<?
}
add_action('admin_menu', 'sp_doclib_create_menu');
