<?
function quantr_tag_initSettingFields() {
    add_settings_section(
        'quantrTag_section_id',	// ID
        'Credential',			// Title
        null,					// Callback
        'Credential'			// Page
    );
	add_settings_field(
	    'quantrTag_name1',            		// ID
	    'Name',                           	// label
	    'quantr_createTextField',   // callback
	    'Credential',               		// page
	    'quantrTag_section_id',             // section ID
		array(
			'quantrTag_name1',
			'text'
		)
	);
	add_settings_field(
	    'quantrTag_domain1',            		// ID
	    'SharePoint domain',                           	// label
	    'quantr_createTextField',             // callback
	    'Credential',               		// page
	    'quantrTag_section_id',             // section ID
		array(
			'quantrTag_domain1',
			'text',
            'If your url is http://<span style=\'color:red\'>quantr</span>.sharepoint.com, then your domain is <span style=\'color:red\'>quantr</span>'
		)
	);
	add_settings_field(
	    'quantrTag_username1',            	// ID
	    'Username',                         // label
	    'quantr_createTextField',   // callback
	    'Credential',               		// page
	    'quantrTag_section_id',             // section ID
		array(
			'quantrTag_username1',
			'text'
		)
	);
	add_settings_field(
	    'quantrTag_password1',            	// ID
	    'Password',                         // label
	    'quantr_createTextField',   // callback
	    'Credential',               		// page
	    'quantrTag_section_id',             // section ID
		array(
			'quantrTag_password1',
			'password'
		)
	);
	add_settings_field(
	    'quantrTag_site1',            	// ID
	    'Site',                         // label
	    'quantr_createTextField',   // callback
	    'Credential',               		// page
	    'quantrTag_section_id',             // section ID
		array(
			'quantrTag_site1',
			'text'
		)
	);

	register_setting('credential_option_group', 'quantrTag_name1'); // option_group, option_name
	register_setting('credential_option_group', 'quantrTag_domain1'); // option_group, option_name
	register_setting('credential_option_group', 'quantrTag_username1'); // option_group, option_name
	register_setting('credential_option_group', 'quantrTag_password1'); // option_group, option_name
	register_setting('credential_option_group', 'quantrTag_site1'); // option_group, option_name
}
function quantr_tag_initSettingQuantrLinkFields() {
    add_settings_section(
        'quantrTag_quantrLink_section_id',	// ID
        'Quantr link',			// Title
        null,					// Callback
        'Quantr link'			// Page
    );
	add_settings_field(
	    'quantrLink_js',            		// ID
	    'Javascript',                           	// label
	    'quantr_createTextField',   // callback
	    'Quantr link',               		// page
	    'quantrTag_quantrLink_section_id',             // section ID
		array(
			'quantrLink_js',
			'textarea'
		)
	);

	register_setting('quantrLink_option_group', 'quantrLink_js'); // option_group, option_name
}

function quantr_createTextField($args) {
	$value = get_option($args[0], '');
	if (isset($args[1]) && $args[1]=='textarea'){
		 echo '<textarea id="'.$args[0].'" name="'.$args[0].'" cols=100 rows=40>'.$value.'</textarea>';
	}else{
		$html = '<input type="'.$args[1].'" id="'.$args[0].'" name="'.$args[0].'" value="'.$value.'" style="width: 300px" />';
	    if (isset($args[2])){
	        $html .= "<br><br><div style='color: black;'>".$args[2]."</div>";
	    }
	    echo $html;
	}
}
add_action('admin_init', 'quantr_tag_initSettingFields');
add_action('admin_init', 'quantr_tag_initSettingQuantrLinkFields');

?>
