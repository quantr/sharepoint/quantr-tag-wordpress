<?
/*
Plugin Name: Quantr tag
Plugin URI: http://www.quantr.hk
Description: Display SharePoint data in wordpress
Version: 1.0.0
Author: Peter
Support URI: http://www.quantr.hk
Author URI: http://peter.quantr.hk
License: GPL2

{Plugin Name} is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

{Plugin Name} is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with {Plugin Name}. If not, see {License URI}.
*/

include(plugin_dir_path(__FILE__) . 'public/shortcode.php');
include(plugin_dir_path(__FILE__) . 'admin/menu.php');
include(plugin_dir_path(__FILE__) . 'admin/setting.php');

/*
wp_deregister_script('jquery');
wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.2.1.js', array(), '3.2.1'); // we should have two versions of jquery, but no way to solve this, peter
wp_enqueue_script('popper.min.js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js', array('jquery'), '1.13.0');
wp_enqueue_script('bootstrap.min.js', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js', array('jquery'), '4.0.0-beta.4');
wp_enqueue_style('bootstrap.min.css', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css');
wp_enqueue_script('picturefill.min.js', 'https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js', array('jquery'), '2.3.1');
wp_enqueue_script('lightgallery-all.min.js', plugin_dir_url(__FILE__) . 'public/js/lightgallery/js/lightgallery.js', array('jquery'), '1.0');
wp_enqueue_script('moment.js', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.js', array('jquery'), '2.20.1');
wp_enqueue_script('lightbox.js', 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.js', array('jquery'), '2.10.0');
wp_enqueue_style('lightbox.css', 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.css');
wp_enqueue_style('quantr-tag-wordpress.css', plugin_dir_url(__FILE__) . 'public/css/quantr-tag-wordpress.css');
*/
?>
