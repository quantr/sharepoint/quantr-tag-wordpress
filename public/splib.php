<?
	function spLogin($username, $password, $domain){
		session_start();
		if (isset($_SESSION['quantr_tag_token_left']) && isset($_SESSION['quantr_tag_token_right']) && isset($_SESSION['quantr_tag_token_formDigestValue'])){
			return array($_SESSION['quantr_tag_token_left'], $_SESSION['quantr_tag_token_right'], $_SESSION['quantr_tag_token_formDigestValue']);
		}else{
			$ch = curl_init();
			//curl_setopt($ch, CURLOPT_URL, "https://".$domain."-sharepointonline.azurewebsites.net/api/getToken?username=$username&password=".urlencode($password)."&domain=".$domain);
			curl_setopt($ch, CURLOPT_URL, "http://quantr.hk:7654/getToken?username=$username&password=".urlencode($password)."&domain=".$domain);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$tokens = curl_exec($ch);

			$ch = curl_init();
			$domain="quantr";
			curl_setopt($ch, CURLOPT_URL, "https://".$domain.".sharepoint.com/test/_api/contextinfo");
			$left=explode("\n", $tokens)[0];
			$right=explode("\n", $tokens)[1];
			//$content.=$left."<br>";
			//$content.=$right."<br>";

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'content-type: application/json;odata=verbose',
				'accept: application/json;odata=verbose',
				'Cookie: '.$left.";".$right,
				'IF-MATCH: *',
				'content-length: 0'));

			$json = json_decode(curl_exec($ch));
			$formDigestValue=$json->{'d'}->{'GetContextWebInformation'}->{'FormDigestValue'};
			curl_close($ch);

			$_SESSION['quantr_tag_token_left']=$left;
			$_SESSION['quantr_tag_token_right']=$right;
			$_SESSION['quantr_tag_token_formDigestValue']=$formDigestValue;
			return array($left, $right, $formDigestValue);
		}
	}

	function get($token, $url){
		$left=$token[0];
		$right=$token[1];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'content-type: application/json;odata=verbose',
			'accept: application/json;odata=verbose',
			'Cookie: '.$left.";".$right,
			'IF-MATCH: *',
			'content-length: 0'));
		$response=curl_exec($ch);
		return $response;
	}

	function getJson($token, $url){
		return json_decode(get($token, $url));
	}

	function download($token, $url){
		$left=$token[0];
		$right=$token[1];
		//echo "\nhttps://quantr.sharepoint.com/%2Ftest%2Fdoclib1%2Fhong-kong-at-sunset-1429622280-DfaO%20%281%29.jpg\n";
		//echo $url."\n";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'content-type: multipart/form-data',
			'Cookie: '.$left.";".$right));
		$response=curl_exec($ch);
		// echo $response;
		// echo "\n-----------------------\n";
		//$response="fuck";
		//echo strlen($response);
		//echo "\n".$response."\n";
		//echo "PETER".base64_encode($response)."PETER";
		return base64_encode($response);
	}
?>
