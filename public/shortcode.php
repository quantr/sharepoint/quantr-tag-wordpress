<?
function quantr_tag_convertToLink($url){
    return "<a style='display: table; margin: auto' href='$url' target=_blank>$url</a>";
}
function getCredential($credentialName){
    for ($x=1;$x<=10;$x++){
        if (get_option('quantrTag_name'.$x, '')!=''){
            return array(get_option('quantrTag_username'.$x, ''), get_option('quantrTag_password'.$x, ''), get_option('quantrTag_domain'.$x, ''), get_option('quantrTag_site'.$x, ''));
        }
    }
}
function quantr_sp_doclib_shortcodes_init()
{
    function quantr_sp_doclib_shortcode($atts = [], $content = null)
    {
        $name=$atts['name'];
        $credentialName=$atts['credential'];
        $view=$atts['view'];
        $doclib=$atts['doclib'];

        if ($credentialName==null){
            $username=$atts['username'];
            $password=$atts['password'];
            $domain=$atts['domain'];
            $site=$atts['site'];
        }else{
            $arr=getCredential($credentialName);
            $username=$arr[0];
            $password=$arr[1];
            $domain=$arr[2];
            $site=$arr[3];
        }
        if ($site=='' || $atts['site']!=''){
            $site=$atts['site'];
        }

        if ($username=='' || $password=='' || $domain==''){
            $content.="<div class='alert alert-danger'>Please specific credential</div>";
        }else{
            $id='quantr_doclib_'.$name;
            $content.=include 'doclib_html.php';
        }
        return $content;
    }
    add_shortcode('quantr_sp_doclib', 'quantr_sp_doclib_shortcode');
}
add_action('init', 'quantr_sp_doclib_shortcodes_init');

function quantr_album_shortcodes_init()
{
    function quantr_album_shortcode($atts = [], $content = null)
    {
        $name=$atts['name'];
        $credentialName=$atts['credential'];

        $arr=getCredential($credentialName);
        $username=$arr[0];
        $password=$arr[1];
        $domain=$arr[2];

        $id='quantr_doclib_'.$name;

        $content.=quantr_tag_convertToLink(plugin_dir_url(__FILE__)."album_html.php");
        return $content;
    }
    add_shortcode('quantr_album', 'quantr_album_shortcode');
}
add_action('init', 'quantr_album_shortcodes_init');


function quantr_list_shortcodes_init()
{
    function quantr_list_shortcode($atts = [], $content = null)
    {
		$name=$atts['name'];
        $credentialName=$atts['credential'];
        $view=$atts['view'];
        $list=$atts['list'];
		$quantrLink=$atts['quantrlink'];

        if ($credentialName==null){
            $username=$atts['username'];
            $password=$atts['password'];
            $domain=$atts['domain'];
            $site=$atts['site'];
        }else{
            $arr=getCredential($credentialName);
            $username=$arr[0];
            $password=$arr[1];
            $domain=$arr[2];
            $site=$arr[3];
        }

        if ($username=='' || $password=='' || $domain==''){
            $content.="<div class='alert alert-danger'>Please specific credential</div>";
        }else{
            $id='quantr_list_'.$name;
            $content.=include 'list_html.php';
        }
        return $content;
    }
    add_shortcode('quantr_list', 'quantr_list_shortcode');
}
add_action('init', 'quantr_list_shortcodes_init');

?>
