<?
	include('commonHeader.php');
?>
<style>
#<?= $id ?>Div input[type=text], input[type=tel], input[type=url], input[type=email], input[type=password], input[type=number]{
	padding: 0px !important;
	padding-left: 12px !important;
	padding-right: 12px !important;
}
#<?= $id ?>Div .input-group-text{
    height: 39px;
}

#<?= $id ?>Div .btn-toolbar:before{
    content: none;
}
#<?= $id ?>Div .btn-toolbar:after{
    content: none;
}

#<?= $id ?>Div td{
	vertical-align: middle;
}
</style>
<script>
$.fn.andSelf = function() {
  return this.addBack.apply(this, arguments);
}
</script>

<div id="<?= $id ?>Div">
    <div class="btn-toolbar justify-content-between" style="margin-bottom: 5px;" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group" role="group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="btnGroupAddon2">Search</div>
                </div>
                <input type="text" class="form-control" placeholder="Input text here" aria-describedby="btnGroupAddon2">
            </div>
            <div class="btn-group" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Upload</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="javascript:$('#uploadFiles').click();">Files</a>
                    <a class="dropdown-item" href="#">Folder</a>
                </div>
            </div>
            <button type="button" class="btn btn-secondary" onclick="changeFolder(currentFolderName);">Refresh</button>
            <button type="button" class="btn btn-secondary">Delete</button>
        </div>

        <div class="input-group">
            <button type="button" class="btn btn-secondary" id="UploadingButton" style="display: none;">Uploading</button>
        </div>
    </div>


    <div id="breadcrumb"></div>
    <table id="mainTable" class="table">
        <thead>
        </thead>
        <tbody>
        </tbody>
    </table>

</div>
<form id="uploadForm" action="<?= plugin_dir_url(__FILE__) ?>doclib_sp.php?type=load" method="post" enctype="multipart/form-data" style="display: none;">
    <input type="file" name="uploadFiles" id="uploadFiles" multiple onchange="javascript:$('#UploadingButton').css('display', '');/*$('#uploadForm').submit();*/">
</form>
<img id='loadingDocLib<?= $id ?>' src='<?= plugin_dir_url(__FILE__) ?>images/Cube.svg' style='display: table; margin: auto; height: 40px;' />
<script>
    $(document).ready(function(){
        lightbox.option({
          'positionFromTop': window.innerHeight/2
        });
    });

    var fieldTypes=new Map();
    var fieldTitles=new Map();
    var viewFields=[];
    var guid;
    var entityTypeName;
    var currentFolderName;

    $.ajax({
        url: "<?= plugin_dir_url(__FILE__) ?>doclib_sp.php?type=guid&username=<?= urlencode($username) ?>&password=<?= urlencode($password) ?>&domain=<?= urlencode($domain) ?>&doclib=<?= urlencode($doclib) ?>&site=<?= urlencode($site) ?>&view=<?= urlencode($view) ?>",
        async: false
    }).done(function(data) {
		try{
	        console.log(JSON.parse(data));
	        guid=JSON.parse(data)['d']['Id'];
	        entityTypeName=JSON.parse(data)['d']['EntityTypeName'];
		}catch(exception){
			$("#<?= $id ?>Div #mainTable tbody").html('<div class="alert alert-danger" role="alert">Error</div>');
            $('#loadingDocLib<?= $id ?>').css('display', 'none');
		}
    });

    $.ajax({
        url: "<?= plugin_dir_url(__FILE__) ?>doclib_sp.php?type=fields&username=<?= urlencode($username) ?>&password=<?= urlencode($password) ?>&domain=<?= urlencode($domain) ?>&doclib=<?= urlencode($doclib) ?>&site=<?= urlencode($site) ?>&view=<?= urlencode($view) ?>",
        async: false
    }).done(function(data) {
        if (data.includes('error')){
            $("#<?= $id ?>Div #mainTable tbody").html('<div class="alert alert-danger" role="alert">'+data+'</div>');
            $('#loadingDocLib<?= $id ?>').css('display', 'none');
        }
        json=JSON.parse(data);

        fields=json['d']['results'];
        for (x=0;x<fields.length;x++){
            //console.log(fields[x]['InternalName']+' = '+fields[x]['FieldTypeKind']);
            fieldTypes[fields[x]['InternalName']]=fields[x]['FieldTypeKind'];
            fieldTitles[fields[x]['InternalName']]=fields[x]['Title'];
        }
    });


    $.ajax({
        url: "<?= plugin_dir_url(__FILE__) ?>doclib_sp.php?type=th&username=<?= urlencode($username) ?>&password=<?= urlencode($password) ?>&domain=<?= urlencode($domain) ?>&doclib=<?= urlencode($doclib) ?>&site=<?= urlencode($site) ?>&view=<?= urlencode($view) ?>&guid="+guid,
        async: false
    }).done(function(data) {
        try{
            cells='';
            headers=JSON.parse(data);
            var header='';
            header+=`<td><input type="checkbox" aria-label="Checkbox for following text input"></td>`;
            for (x=0;x<headers.length;x++){
                viewFields.push(headers[x]);
                //fieldType=fieldTypes[headers[x]];
                header+='<td>'+fieldTitles[headers[x]]+'</td>';
            }
            header+=`<td></td>`;
            $("#<?= $id ?>Div #mainTable thead").html(header);
        }catch(err){
            cells+='<tr><td class="alert alert-danger"><strong>Error! </strong> '+err+'</td><tr>';
            cells+='<tr><td class="alert alert-danger"><strong>Data: </strong> '+data+'</td><tr>';
            $("#<?= $id ?>Div #mainTable tbody").html(cells);
            $('#loadingDocLib<?= $id ?>').css('display', 'none');
        }
    });

    function isImage(extension){
        extension=extension.toLowerCase();
        switch(extension){
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
            case 'png':
                return true;
            default:
                return false;
        }
    }

    changeFolder('');

    function changeFolder(folderName){
        currentFolderName=folderName;
        $('#loadingDocLib<?= $id ?>').css('display', 'block');
        $("#<?= $id ?>Div #mainTable tbody").empty();
        $.ajax({
            url: "<?= plugin_dir_url(__FILE__) ?>doclib_sp.php?type=td&username=<?= urlencode($username) ?>&password=<?= urlencode($password) ?>&domain=<?= urlencode($domain) ?>&doclib=<?= urlencode($doclib) ?>&entityTypeName="+entityTypeName+"&site=<?= urlencode($site) ?>&view=<?= urlencode($view) ?>&headers="+JSON.stringify(viewFields)+"&fieldTypes="+JSON.stringify(fieldTypes)+'&folderName='+folderName+'&guid='+guid
        }).done(function(data) {
            try{
                if (data==''){
                }else if (JSON.parse(data)['error']!=null){
                    errorMsg=JSON.parse(data)['error'];
                    cells+='<tr><td colspan='+headers.length+' class="alert alert-danger"><strong>'+errorMsg['code']+'</strong> '+errorMsg['message']['value']+'</td><tr>';
                }else{
                    json=JSON.parse(data)['d']['results'];
                    console.log(json);
                    cells='';
                    for (y=0;y<json.length;y++){
                        cells+='<tr>';
                        cells+=`<td><input type="checkbox" aria-label="Checkbox for following text input"></td>`;
                        for (x=0;x<headers.length;x++){
							console.log(json[y]);
                            temp=json[y][headers[x]];
                            uniqueId=json[y]['UniqueId'];
                            fieldType=fieldTypes[headers[x]];

                            if (headers[x]=="DocIcon"){
                                if (temp==null){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/famfamfam/folder.png" /></td>';
                                }else if(isImage(temp)){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/famfamfam/image.png" /></td>';
                                }else if(temp=='ai'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/ai.svg" style="height: 20px;" /></td>';
                                }else if(temp=='doc' || temp=='docx'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/doc.svg" style="height: 20px;" /></td>';
                                }else if(temp=='exe'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/exe.svg" style="height: 20px;" /></td>';
                                }else if(temp=='js'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/js.svg" style="height: 20px;" /></td>';
                                }else if(temp=='json'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/json.svg" style="height: 20px;" /></td>';
                                }else if(temp=='mp3'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/mp3.svg" style="height: 20px;" /></td>';
                                }else if(temp=='mp4'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/mp4.svg" style="height: 20px;" /></td>';
                                }else if(temp=='pdf'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/pdf.svg" style="height: 20px;" /></td>';
                                }else if(temp=='ppt' || temp=='pptx'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/ppt.svg" style="height: 20px;" /></td>';
                                }else if(temp=='psd'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/psd.svg" style="height: 20px;" /></td>';
                                }else if(temp=='svg'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/svg.svg" style="height: 20px;" /></td>';
                                }else if(temp=='txt'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/txt.svg" style="height: 20px;" /></td>';
                                }else if(temp=='xls' || temp=='xlsx'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/xls.svg" style="height: 20px;" /></td>';
                                }else if(temp=='zip'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/zip.svg" style="height: 20px;" /></td>';
                                }else{
                                    cells+='<td></td>';
                                }
                            }else if (headers[x]=='LinkFilename'){
                                if (json[y]['DocIcon']==null){
                                    cells+='<td><a style="cursor:pointer" onclick="changeFolder(\''+folderName+'/'+temp+'\',\''+uniqueId+'\');">'+temp+'</a></td>';
                                }else{
                                    cells+='<td><a style="cursor:pointer" onclick="showPopup(\''+folderName+'\',\''+temp+'\',\''+uniqueId+'\');">'+temp+'</a></td>';
                                }
                            }else{
                                if (temp==null){
                                    cells+='<td></td>';
                                }else{
                                    if (fieldType==4){
                                        tempDate=moment(temp);
                                        cells+='<td>'+tempDate.format('YYYY/MM/DD')+'</td>';
                                    }else{
                                        if (typeof temp === 'object' && temp!=null){
                                            cells+='<td>'+temp['Title']+'</td>';
                                        }else{
                                            cells+='<td>'+temp+'</td>';
                                        }
                                    }
                                }
                            }
                        }
						cells+=`
							<td>
								<div class="btn-group" role="group">
									<button id="btnGroupDrop1" type="button" class="btn btn-light btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
									<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
										<a class="dropdown-item" href=# onclick="downloadFile(\'`+folderName+`\',\'`+temp+`\',\'`+uniqueId+`\');">Download</a>
										<a class="dropdown-item" href=#>Log</a>
									</div>
								</div>
							</td>
						`
                        cells+='</tr>';
                    }
                }
            }catch(err){
                cells+='<tr><td colspan='+headers.length+' class="alert alert-danger"><strong>Error! </strong> '+err+'</td><tr>';
                cells+='<tr><td colspan='+headers.length+' class="alert alert-danger"><strong>Data: </strong> '+data+'</td><tr>';
            }

            // breadcrumb
            if (folderName==''){
                breadcrumb=`
                    <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page"><a href=# onclick="changeFolder('');"><?= $doclib ?></a></li>
                      </ol>
                    </nav>`;
            }else{
                breadcrumb=`
                    <nav aria-label="breadcrumb" role="navigation">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href=# onclick="changeFolder('');"><?= urlencode($doclib) ?></a></li>`;
                folders=folderName.split('/');
                currentFolder='';
                for (x=1;x<folders.length-1;x++){
                    currentFolder+='/'+folders[x];
                    breadcrumb+=`<li class="breadcrumb-item"><a href=# onclick="changeFolder('`+currentFolder+`');">`+folders[x]+`</a></li>`;
                }
                breadcrumb+=`<li class="breadcrumb-item active" aria-current="page"><a href=# onclick="changeFolder('`+currentFolder+`');">`+folders[folders.length-1]+`</a></li>`;
                breadcrumb+=`
                      </ol>
                    </nav>`;
            }
            $("#<?= $id ?>Div #breadcrumb").html(breadcrumb);
            $("#<?= $id ?>Div #mainTable tbody").html(cells);
            $('#loadingDocLib<?= $id ?>').css('display', 'none');
        });
    }

    function showPopup(folderName, filename, uniqueId){
        extension=filename.split('.').pop();
        $('#modalImg').css('display', 'none');
        $('#modalEmbed').css('display', 'none');
        $('#modalImg').attr('src', '');
        $('#myModal').css('top', '100px');
        $('#loadingDocLib<?= $id ?>_dialog').css('display', 'block');
        $('#myModal').modal();
        $.ajax({
            url: "<?= plugin_dir_url(__FILE__) ?>doclib_sp.php?type=downloadFile&username=<?= urlencode($username) ?>&password=<?= urlencode($password) ?>&domain=<?= urlencode($domain) ?>&doclib="+entityTypeName+"&site=<?= urlencode($site) ?>&uniqueId="+uniqueId+"&folderName="+folderName+"&entityTypeName="+entityTypeName
        }).done(function(data) {
            if (isImage(extension)){
                $('#modalImg').attr('src', 'data:image/'+extension+';base64,'+data);
                $('#modalImg').css('display', 'block');
            }else{
                $('.modal-dialog').css('max-width', '750px');
                $('#modalEmbed').attr('src', 'data:application/'+extension+';base64,'+data);
                $('#modalEmbed').css('display', 'block');
            }
            $('#loadingDocLib<?= $id ?>_dialog').css('display', 'none');
        });
    }

	function downloadFile(folderName, filename, uniqueId){
		$.ajax({
            url: "<?= plugin_dir_url(__FILE__) ?>doclib_sp.php?type=getDownloadUrl&username=<?= urlencode($username) ?>&password=<?= urlencode($password) ?>&domain=<?= urlencode($domain) ?>&doclib="+entityTypeName+"&site=<?= urlencode($site) ?>&uniqueId="+uniqueId+"&folderName="+folderName+"&entityTypeName="+entityTypeName
        }).done(function(data) {
			window.open('<?='https://'.$domain.'.sharepoint.com/' ?>/'+data);
		});
	}
</script>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <img id="modalImg" style="margin: auto; display: none;" />
          <embed id="modalEmbed" type='application/pdf' style="margin: auto; display: none; width: 700px; height: 800px" />
          <img id='loadingDocLib<?= $id ?>_dialog' src='<?= plugin_dir_url(__FILE__) ?>images/Cube.svg' style='display: block; margin: auto; height: 200px;' />
      </div>
    </div>
  </div>
</div>
<?
    return '';
?>
