<?
	include('commonLibHeader.php');
?>
<style>
#<?= $id ?>Div input[type=text], input[type=tel], input[type=url], input[type=email], input[type=password], input[type=number]{
	padding: 0px !important;
	padding-left: 12px !important;
	padding-right: 12px !important;
}
#<?= $id ?>Div .input-group-text{
    height: 39px;
}

#<?= $id ?>Div .btn-toolbar:before{
    content: none;
}
#<?= $id ?>Div .btn-toolbar:after{
    content: none;
}

#<?= $id ?>Div td{
	vertical-align: middle;
}
</style>
<script>
$.fn.andSelf = function() {
  return this.addBack.apply(this, arguments);
}
<?= get_option('quantrLink_js', '') ?>
</script>

<div id="<?= $id ?>Div">
    <table id="mainTable" class="table">
        <thead>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<form id="uploadForm" action="<?= plugin_dir_url(__FILE__) ?>list_sp.php?type=load" method="post" enctype="multipart/form-data" style="display: none;">
    <input type="file" name="uploadFiles" id="uploadFiles" multiple onchange="javascript:$('#UploadingButton').css('display', '');/*$('#uploadForm').submit();*/">
</form>
<img id='loadingList<?= $id ?>' src='<?= plugin_dir_url(__FILE__) ?>images/Cube.svg' style='display: table; margin: auto; height: 40px;' />
<script>
    $(document).ready(function(){
        lightbox.option({
          'positionFromTop': window.innerHeight/2
        });
    });

    var fieldTypes=new Map();
    var fieldTitles=new Map();
    var viewFields=[];
    var guid;
    var entityTypeName;

    $.ajax({
        url: "<?= plugin_dir_url(__FILE__) ?>list_sp.php?type=guid&username=<?= urlencode($username) ?>&password=<?= urlencode($password) ?>&domain=<?= urlencode($domain) ?>&list=<?= urlencode($list) ?>&site=<?= urlencode($site) ?>&view=<?= urlencode($view) ?>",
        async: false
    }).done(function(data) {
        //console.log(data);
        guid=JSON.parse(data)['d']['Id'];
        entityTypeName=JSON.parse(data)['d']['EntityTypeName'];
    });

    $.ajax({
        url: "<?= plugin_dir_url(__FILE__) ?>list_sp.php?type=fields&username=<?= urlencode($username) ?>&password=<?= urlencode($password) ?>&domain=<?= urlencode($domain) ?>&list=<?= urlencode($list) ?>&site=<?= urlencode($site) ?>&view=<?= urlencode($view) ?>",
        async: false
    }).done(function(data) {
        if (data.includes('error')){
            $("#<?= $id ?>Div #mainTable tbody").html('<div class="alert alert-danger" role="alert">'+data+'</div>');
            $('#loadingList<?= $id ?>').css('display', 'none');
        }
        json=JSON.parse(data);

        fields=json['d']['results'];
        for (x=0;x<fields.length;x++){
            //console.log(fields[x]['InternalName']+' = '+fields[x]['FieldTypeKind']);
            fieldTypes[fields[x]['InternalName']]=fields[x]['FieldTypeKind'];
            fieldTitles[fields[x]['InternalName']]=fields[x]['Title'];
        }
    });

    $.ajax({
        url: "<?= plugin_dir_url(__FILE__) ?>list_sp.php?type=th&username=<?= urlencode($username) ?>&password=<?= urlencode($password) ?>&domain=<?= urlencode($domain) ?>&list=<?= urlencode($list) ?>&site=<?= urlencode($site) ?>&view=<?= urlencode($view) ?>&guid="+guid,
        async: false
    }).done(function(data) {
        try{
            cells='';
            headers=JSON.parse(data);
            var header='';
            for (x=0;x<headers.length;x++){
                viewFields.push(headers[x]);
                //fieldType=fieldTypes[headers[x]];
                header+='<td>'+fieldTitles[headers[x]]+'</td>';
            }
            $("#<?= $id ?>Div #mainTable thead").html(header);
        }catch(err){
            cells+='<tr><td class="alert alert-danger"><strong>Error! </strong> '+err+'</td><tr>';
            cells+='<tr><td class="alert alert-danger"><strong>Data: </strong> '+data+'</td><tr>';
            $("#<?= $id ?>Div #mainTable tbody").html(cells);
            $('#loadingList<?= $id ?>').css('display', 'none');
        }
    });

    function isImage(extension){
        extension=extension.toLowerCase();
        switch(extension){
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
            case 'png':
                return true;
            default:
                return false;
        }
    }

    loadList();

    function loadList(){
        $('#loadingList<?= $id ?>').css('display', 'block');
        $("#<?= $id ?>Div #mainTable tbody").empty();
        $.ajax({
            url: "<?= plugin_dir_url(__FILE__) ?>list_sp.php?type=td&username=<?= urlencode($username) ?>&password=<?= urlencode($password) ?>&domain=<?= urlencode($domain) ?>&list=<?= urlencode($list) ?>&entityTypeName="+entityTypeName+"&site=<?= urlencode($site) ?>&view=<?= urlencode($view) ?>&headers="+JSON.stringify(viewFields)+"&fieldTypes="+JSON.stringify(fieldTypes)+'&guid='+guid
        }).done(function(data) {
            try{
                if (data==''){
                }else if (JSON.parse(data)['error']!=null){
					console.log('error 1');
                    errorMsg=JSON.parse(data)['error'];
                    cells+='<tr><td colspan='+headers.length+' class="alert alert-danger"><strong>'+errorMsg['code']+'</strong> '+errorMsg['message']['value']+'</td><tr>';
                }else{
                    json=JSON.parse(data)['d']['results'];
					console.log(json);
                    cells='';
					quantrScripts='';
                    for (y=0;y<json.length;y++){
                        cells+='<tr>';
                        for (x=0;x<headers.length;x++){
                            temp=json[y][headers[x]];
                            uniqueId=json[y]['UniqueId'];
                            fieldType=fieldTypes[headers[x]];

                            if (headers[x]=="DocIcon"){
                                if (temp==null){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/famfamfam/folder.png" /></td>';
                                }else if(isImage(temp)){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/famfamfam/image.png" /></td>';
                                }else if(temp=='ai'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/ai.svg" style="height: 20px;" /></td>';
                                }else if(temp=='doc' || temp=='docx'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/doc.svg" style="height: 20px;" /></td>';
                                }else if(temp=='exe'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/exe.svg" style="height: 20px;" /></td>';
                                }else if(temp=='js'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/js.svg" style="height: 20px;" /></td>';
                                }else if(temp=='json'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/json.svg" style="height: 20px;" /></td>';
                                }else if(temp=='mp3'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/mp3.svg" style="height: 20px;" /></td>';
                                }else if(temp=='mp4'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/mp4.svg" style="height: 20px;" /></td>';
                                }else if(temp=='pdf'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/pdf.svg" style="height: 20px;" /></td>';
                                }else if(temp=='ppt' || temp=='pptx'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/ppt.svg" style="height: 20px;" /></td>';
                                }else if(temp=='psd'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/psd.svg" style="height: 20px;" /></td>';
                                }else if(temp=='svg'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/svg.svg" style="height: 20px;" /></td>';
                                }else if(temp=='txt'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/txt.svg" style="height: 20px;" /></td>';
                                }else if(temp=='xls' || temp=='xlsx'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/xls.svg" style="height: 20px;" /></td>';
                                }else if(temp=='zip'){
                                    cells+='<td align="center"><img src="<?= plugin_dir_url(__FILE__) ?>images/fileicon/zip.svg" style="height: 20px;" /></td>';
                                }else{
                                    cells+='<td></td>';
                                }
                            }else{
								cellId=Math.ceil(Math.random()*100000000);
								if (fieldType==4){
									tempDate=moment(temp);
									cells+='<td>'+tempDate.format('YYYY/MM/DD')+'</td>';
								}else{
									if (typeof temp === 'object' && temp!=null){
										cells+='<td>'+temp['Title']+'</td>';
									}else{
										console.log("> "+temp+", "+(typeof temp));
										cells+='<td>'+<?= $quantrLink ?>(headers[x], temp)+'</td>';
									}
								}
                            }
                        }
                        cells+='</tr>';
                    }
                }
            }catch(err){
                cells+='<tr><td colspan='+headers.length+' class="alert alert-danger"><strong>Error! </strong> '+err+'</td><tr>';
                cells+='<tr><td colspan='+headers.length+' class="alert alert-danger"><strong>Data: </strong> '+data+'</td><tr>';
            }
            $("#<?= $id ?>Div #mainTable tbody").html(cells);
            $('#loadingList<?= $id ?>').css('display', 'none');
        });
    }
</script>
<?
    return '';
?>
