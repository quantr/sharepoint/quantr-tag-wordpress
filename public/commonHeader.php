<script type='text/javascript' src='https://code.jquery.com/jquery-3.3.1.js'></script>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js'></script>
<script type='text/javascript' src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'></script>
<link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' type='text/css' rel='stylesheet' />
<script type='text/javascript' src='https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js'></script>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.js'></script>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.js'></script>
<link href='<?= plugin_dir_url(__FILE__) ?>/css/quantr-tag-wordpress.css' type='text/css' rel='stylesheet' />
<script>
$.fn.andSelf = function() {
  return this.addBack.apply(this, arguments);
}
</script>
