<?
	include('splib.php');

	$token=spLogin($_GET['username'], $_GET['password'], $_GET['domain']);

	if ($_GET['type']=='th'){
		echo processTH($token);
	}else if ($_GET['type']=='td'){
		echo processTD($token);
	}else if ($_GET['type']=='fields'){
		echo fields($token);
	}else if ($_GET['type']=='getFile'){
		echo getFile($token, $_GET['uniqueId']);
	}else if ($_GET['type']=='guid'){
		echo guid($token, $_GET['uniqueId']);
	}else if ($_GET['type']=='downloadFile'){
		echo downloadFile($token, $_GET['uniqueId']);
	}else if ($_GET['type']=='getDownloadUrl'){
		echo getDownloadUrl($token, $_GET['uniqueId']);
	}else if ($_GET['type']=='upload'){
		echo "upload";
		die;
	}else{
		echo "error type = ".$_GET['type'];
	}
	
	function guid($token){
		$left=$token[0];
		$right=$token[1];
		$formDigestValue=$arr[2];

		$json=get($token, 'https://'.$_GET['domain'].'.sharepoint.com/'.$_GET['site'].'/_api/web/lists/GetByTitle(\''.rawurlencode($_GET['doclib']).'\')?$select=Id,EntityTypeName');
		return $json;
	}

	function fields($token){
		$left=$token[0];
		$right=$token[1];
		$formDigestValue=$arr[2];

		$json=get($token, 'https://'.$_GET['domain'].'.sharepoint.com/'.$_GET['site'].'/_api/web/lists/GetByTitle(\''.rawurlencode($_GET['doclib']).'\')/fields');
		return $json;
	}

	function processTH($token){
		if ($_GET['view']==''){
			$_GET['view']='All Documents';
		}
		$url='https://'.$_GET['domain'].'.sharepoint.com/'.$_GET['site'].'/_api/web/lists(guid\''.rawurlencode($_GET['guid']).'\')/views/getbytitle(\''.rawurlencode($_GET['view']).'\')/ViewFields';
		//echo $url;
		$json = getJson($token, $url);
		//var_dump($json);
		$fields=$json->{'d'}->{'Items'}->{'results'};

		$headers='';
		for ($x=0;$x<count($fields);$x++){
			$headers[]=$fields[$x];
		}
		return json_encode($headers);
	}

	function processTD($token){
		$fieldTypes=json_decode($_GET['fieldTypes']);
		$headers=json_decode($_GET['headers']);
		$expand='&$expand=';

		$url='https://'.$_GET['domain'].'.sharepoint.com/'.$_GET['site'].'/_api/web/getfolderbyserverrelativeurl(\'/'.rawurlencode($_GET['site'].'/'.str_replace('_x0020_','%20',$_GET['entityTypeName']).'/'.$_GET['folderName']).'\')/folders?$expand=ListItemAllFields&$filter='.rawurlencode('ListItemAllFields/GUID ne null');
		//echo $url;
		$json=getJson($token, $url);
		$rows=$json->{'d'}->{'results'};
		//$guids=[];
		for ($x=0;$x<count($rows);$x++){
			$guids[]=$rows[$x]->{'ListItemAllFields'}->{'GUID'};
		}

		$url='https://'.$_GET['domain'].'.sharepoint.com/'.$_GET['site'].'/_api/web/getfolderbyserverrelativeurl(\'/'.rawurlencode($_GET['site'].'/'.str_replace('_x0020_','%20',$_GET['entityTypeName']).'/'.$_GET['folderName']).'\')/files?$expand=ListItemAllFields';
		$json=getJson($token, $url);
		$rows=$json->{'d'}->{'results'};
		for ($x=0;$x<count($rows);$x++){
			$guids[]=$rows[$x]->{'ListItemAllFields'}->{'GUID'};
		}
		if ($guids==null){
			return '';
		}
		foreach($guids as $guid){
			if ($filters!='') {
				$filters.=" or";
			}
			$filters.=" GUID eq guid'".$guid."'";
		}
		//var_dump($guids);

		for ($x=0;$x<count($headers);$x++){
			$fieldName = $headers[$x];
			if ($fieldTypes->{$fieldName} == 20) {
				$query.=$fieldName."/Title,";
				$expand.=$fieldName.",";
			} else {
				$query.=$fieldName.",";
			}
		}
		$query.="UniqueId,";
		if ($query[strlen($query)-1]==','){
			$query=substr($query, 0, strlen($query)-1);
		}
		if ($expand[strlen($expand)-1]==','){
			$expand=substr($expand, 0, strlen($expand)-1);
		}

		$url='https://'.$_GET['domain'].'.sharepoint.com/'.$_GET['site'].'/_api/web/lists/GetByTitle(\''.rawurlencode($_GET['doclib']).'\')/items?$select='.$query.$expand.'&$filter='.rawurlencode($filters);
		//echo $url."\n";
		$response=get($token, $url);
		//echo $response;
		return $response;
	}

	function getFile($token, $uniqueId){
		$url='https://'.$_GET['domain'].'.sharepoint.com/'.$_GET['site'].'/_api/web/getfolderbyserverrelativeurl(\''.rawurlencode($_GET['doclib']).'\')/files?$filter='.urlencode('UniqueId eq guid\''.$uniqueId.'\'');
		$response = get($token, $url);
		return $response;
	}

	function downloadFile($token, $uniqueId){
		$url='https://'.$_GET['domain'].'.sharepoint.com/'.$_GET['site'].'/_api/web/getfolderbyserverrelativeurl(\''.rawurlencode(str_replace('_x0020_','%20',$_GET['entityTypeName']).'/'.$_GET['folderName']).'\')/files?$filter='.urlencode('UniqueId eq guid\''.$uniqueId.'\'');
		//echo $url;
		$json =getJson($token, $url);
		$url=$json->{'d'}->{'results'}[0]->{'ServerRelativeUrl'};
		//echo 'https://'.$_GET['domain'].'.sharepoint.com/'.rawurlencode($url);
		$response=download($token, 'https://'.$_GET['domain'].'.sharepoint.com/'.rawurlencode($url));
		return $response;
	}

	function getDownloadUrl($token, $uniqueId){
		$url='https://'.$_GET['domain'].'.sharepoint.com/'.$_GET['site'].'/_api/web/getfolderbyserverrelativeurl(\''.rawurlencode(str_replace('_x0020_','%20',$_GET['entityTypeName']).'/'.$_GET['folderName']).'\')/files?$filter='.urlencode('UniqueId eq guid\''.$uniqueId.'\'');
		$json =getJson($token, $url);
		$url=$json->{'d'}->{'results'}[0]->{'ServerRelativeUrl'};
		return $url;
	}
?>
