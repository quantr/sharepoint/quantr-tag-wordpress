<?
	include('splib.php');

	$token=spLogin($_GET['username'], $_GET['password'], $_GET['domain']);

	if ($_GET['type']=='th'){
		echo processTH($token);
	}else if ($_GET['type']=='td'){
		echo processTD($token);
	}else if ($_GET['type']=='fields'){
		echo fields($token);
	}else if ($_GET['type']=='getFile'){
		echo getFile($token, $_GET['uniqueId']);
	}else if ($_GET['type']=='guid'){
		echo guid($token, $_GET['uniqueId']);
	}else if ($_GET['type']=='downloadFile'){
		echo downloadFile($token, $_GET['uniqueId']);
	}else if ($_GET['type']=='upload'){
		echo "upload";
		die;
	}else{
		echo "error type = ".$_GET['type'];
	}

	function guid($token){
		$left=$token[0];
		$right=$token[1];
		$formDigestValue=$arr[2];

		$json=get($token, 'https://'.$_GET['domain'].'.sharepoint.com/'.$_GET['site'].'/_api/web/lists/GetByTitle(\''.rawurlencode($_GET['list']).'\')?$select=Id,EntityTypeName');
		return $json;
	}

	function fields($token){
		$left=$token[0];
		$right=$token[1];
		$formDigestValue=$arr[2];

		$json=get($token, 'https://'.$_GET['domain'].'.sharepoint.com/'.$_GET['site'].'/_api/web/lists/GetByTitle(\''.rawurlencode($_GET['list']).'\')/fields');
		return $json;
	}

	function processTH($token){
		if ($_GET['view']==''){
			$_GET['view']='All Items';
		}
		$url='https://'.$_GET['domain'].'.sharepoint.com/'.$_GET['site'].'/_api/web/lists(guid\''.rawurlencode($_GET['guid']).'\')/views/getbytitle(\''.rawurlencode($_GET['view']).'\')/ViewFields';
		//echo $url;
		$json = getJson($token, $url);
		//var_dump($json);
		$fields=$json->{'d'}->{'Items'}->{'results'};

		$headers='';
		for ($x=0;$x<count($fields);$x++){
			$headers[]=$fields[$x];
		}
		return json_encode($headers);
	}

	function processTD($token){
		$fieldTypes=json_decode($_GET['fieldTypes']);
		$headers=json_decode($_GET['headers']);
		$expand='&$expand=';

		for ($x=0;$x<count($headers);$x++){
			$fieldName = $headers[$x];
			//echo $fieldName."=".$fieldTypes->{$fieldName}."<br>";
			if ($fieldTypes->{$fieldName} == 20 || $fieldTypes->{$fieldName} == 7) {
				$query.=$fieldName."/Title,";
				$expand.=$fieldName.",";
			} else {
				$query.=$fieldName.",";
			}
		}
		$query.="UniqueId,";
		if ($query[strlen($query)-1]==','){
			$query=substr($query, 0, strlen($query)-1);
		}
		if ($expand[strlen($expand)-1]==','){
			$expand=substr($expand, 0, strlen($expand)-1);
		}

		$url='https://'.$_GET['domain'].'.sharepoint.com/'.$_GET['site'].'/_api/web/lists/GetByTitle(\''.rawurlencode($_GET['list']).'\')/items?$select='.$query.$expand;
		//echo $url."\n";
		$response=get($token, $url);
		//echo $response;
		return $response;
	}
?>
