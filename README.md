![banner](http://www.quantr.hk/wp-content/uploads/2017/11/banner1.png)

# Introduction

Wordpress plugin for Quantr tag. Quantr tag is a special [short code] to display SharePoint data on wordpress.

# Document library

[quantr_doclib credential=Quantr doclib=doclib]1

[quantr_doclib credential=Quantr doclib=doclib1 view=xxx width=1000px]

[quantr_doclib credential=Quantrdoclib=doclib1 width=1000px caml='<View><ViewFields><FieldRef Name="Title" /><FieldRef Name="Column2" /></ViewFields></View>']

[quantr_doclib credential=Quantrdoclib=doclib1 width=1000px caml='<Query><Where><Eq><FieldRef Name="Title" /><Value Type="Text">ABC VALUE</Value></Eq></Where></Query>']

# Album

![](https://preview.ibb.co/cWDN76/Screen_Shot_2017_12_15_at_2_22_07_PM.png)

![](https://preview.ibb.co/htMg0R/Screen_Shot_2017_12_15_at_2_23_03_PM.png)

[quantr_album doclib=doclib1 type=default grid=4x4]

[quantr_album doclib=doclib1 type=default grid=4x4 view=view1]

[quantr_album doclib=doclib1 type=default grid=4x4 caml='<Query><Where><Eq><FieldRef Name="Title" /><Value Type="Text">ABC VALUE</Value></Eq></Where></Query>']

# Corporate News

[quantr_news list=list1 type=vertical maxRow=10]

[quantr_news list=list1 type=vertical maxRow=10 showAttachment]

[quantr_news list=list1 type=vertical maxRow=10 showAttachment view=view1]

[quantr_news list=list1 type=vertical maxRow=10 showAttachment caml='<Query><Where><Eq><FieldRef Name="Title" /><Value Type="Text">ABC VALUE</Value></Eq></Where></Query>'] 


